<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=LXGW+WenKai+Mono+TC&display=swap" rel="stylesheet">
    <title>Pedidos</title>
</head>
<body>
    <section class="form-register">
        <form action="user.php" method="POST">
            <p class="titulo">MENU</p>
            <div class="contenedor-items">
            <?php
            $conn = mysqli_connect("localhost", "root", "", "ej2");

            if (!$conn) {
                die("Conexión Fallida: " . mysqli_connect_error());
            }

            $sql = "SELECT id, nombre_prod, precio FROM productos";
            $result = mysqli_query($conn, $sql);

            if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    echo '<div class="producto">';
                    echo '<input class="check-prod" type="checkbox" name="items[]" value="' . $row["precio"] . '"> ';
                    echo '<span class="nombre-prod">' . $row["nombre_prod"] . "</span>: $<span class='precio-prod'>" . $row["precio"] . "</span><br>";
                    echo'</div>';
                }
            } else {
                echo "0 results";
            }

            mysqli_close($conn);
            ?>
            <p class="sub">Tipo de entrega</p>
            <div>
                <label class="label">
                    <input class="check-prod" type="checkbox" name="retirar" value="0"> Retirar 
                </label>
            </div>
            <div>
                <label class="label">
                    <input class="check-prod" type="checkbox" name="delivery" value="5"> Delivery (Costo $5)
                </label> 
            </div>
            </div> 
            <input class="boton" type="submit" value="Realizar pedido">
        </form>
    </section>
</body>
</html>

