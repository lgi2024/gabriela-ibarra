<?php
include("conexion.php");
$con = connection();
$total = 0;

// Recoger los datos del usuario y del pedido
$name = isset($_GET['name']) ? htmlspecialchars($_GET['name']) : '';
$items = isset($_GET['items']) ? explode(",", $_GET['items']) : [];
$delivery = isset($_GET['delivery']) ? (int)$_GET['delivery'] : 0;

// Calcular el total
foreach ($items as $item) {
    $total += (int)$item;
}

$total += $delivery;

echo "<html>
          <head>
            <title>Resultado</title>
            <link rel='stylesheet' href='index.css'>
            <link rel='preconnect' href='https://fonts.googleapis.com'>
            <link rel='preconnect' href='https://fonts.gstatic.com' crossorigin>
            <link href='https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap' rel='stylesheet'>
           </head>
           <body>
                <section class='form-user'>
                   <p>Bienvenido $name</p>
                   <p>Total del pedido: $" . $total . "</p>                    
                </section>
            </body>
       </html>";
?>
