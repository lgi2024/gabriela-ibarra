<?php
include("conexion.php");
$con = connection();

$error = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = isset($_POST['name']) ? mysqli_real_escape_string($con, $_POST['name']) : '';
    $password = isset($_POST['con']) ? mysqli_real_escape_string($con, $_POST['con']) : '';
    $action = isset($_POST['action']) ? $_POST['action'] : '';

    if ($action === 'Registrar') {
        // Registro
        $sql = "INSERT INTO usuario (name, password) VALUES ('$name', '$password')";
        if (mysqli_query($con, $sql)) {
            $error = "Usuario registrado exitosamente.";
        } else {
            $error = "Error: " . $sql . "<br>" . mysqli_error($con);
        }
    } elseif ($action === 'Iniciar') {
        // Inicio de sesión
        $sql = "SELECT * FROM usuario WHERE name='$name' AND password='$password'";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0) {
            $items = isset($_POST['items']) ? $_POST['items'] : [];
            $delivery = isset($_POST['delivery']) ? $_POST['delivery'] : 0;
            $items_str = implode(",", $items);
            // Redirigir a pedido.php con parámetros GET
            header("Location: pedido.php?name=" . urlencode($name) . "&items=" . urlencode($items_str) . "&delivery=" . urlencode($delivery));
            exit;
        } else {
            $error = "Usuario no existe.";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=LXGW+WenKai+Mono+TC&display=swap" rel="stylesheet">
    <title>Formulario</title>
</head>
<body>
    <section class="form-user">
        <form action="user.php" method="POST">
            <div>
                <input class="controls" type="text" size="25" name="name" placeholder="Nombre" required />
            </div>
            <div>
                <input class="controls" type="password" size="5" name="con" placeholder="Contraseña" required />
            </div>
            <div class="btn">
                <button type="submit" name="action" value="Iniciar">Iniciar sesión</button>
                <button type="submit" class="btn2" name="action" value="Registrar">Registrarse</button>
            </div>
            <?php
            if (isset($_POST['items'])) {
                foreach ($_POST['items'] as $item) {
                    echo '<input type="hidden" name="items[]" value="' . htmlspecialchars($item) . '">';
                }
            }
            if (isset($_POST['delivery'])) {
                echo '<input type="hidden" name="delivery" value="' . htmlspecialchars($_POST['delivery']) . '">';
            }
            ?>
        </form>
        <?php if ($error): ?>
            <div class="error">
                <?php echo htmlspecialchars($error); ?>
            </div>
        <?php endif; ?>
    </section>
</body>
</html>


