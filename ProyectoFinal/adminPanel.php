<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "finalproject";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$search = '';
if (isset($_GET['search']) && !empty($_GET['search'])) {
    $search = mysqli_real_escape_string($conn, $_GET['search']);
    $sql = "SELECT * FROM form WHERE nombre LIKE '%$search%' OR estado LIKE '%$search%' OR fecha_envio = '$search'";
} else {
    $sql = "SELECT id, nombre, apellido, DATE_FORMAT(fecha_envio, '%Y-%m-%d') AS fecha_envio, estado FROM form";
}

$result = $conn->query($sql);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="img/png" href="img/logo.png">
    <title>Panel de administración</title>
    <link rel="stylesheet" href="css/adminPanel.css">
</head>
<body>
    <header>
        <div class="logo"></div>
        <nav>
            <button onclick="toggleAccount()">Mi cuenta</button>
            <button>Reportes</button>
            <a href="login.php">Cerrar sesión</a>
        </nav>
    </header>

    <h1>Lista de Formularios</h1>

    <div class="search-bar">
        <form method="GET" action="">
            <input type="text" name="search" placeholder="Buscar" value="<?php echo htmlspecialchars($search); ?>">
            <input type="submit" value="Buscar">
        </form>
    </div>

    <table>
        <thead>
            <tr>
                <th>N°</th>
                <th>Nombre</th>
                <th>Fecha de Envío</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['nombre'] . " " . $row['apellido'] . "</td>";
                    echo "<td>" . $row['fecha_envio'] . "</td>";
                    $status_class = ($row['estado'] == 'Pendiente') ? 'status-icon red' : 'status-icon green';
                    echo "<td><span class='$status_class'>&#128503;</span> " . $row['estado'] . "</td>";
                    echo "<td>
                            <button class='action-btn view-btn' onclick='openModal(" . $row['id'] . ")'>👁️</button>
                            <button class='action-btn download-btn'>⬇️</button>
                            <a href='delete.php?id=" . $row['id'] . "'>🗑️</a>
                        </td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr><td colspan='5'>No hay formularios enviados.</td></tr>";
            }
            $conn->close();
            ?>
        </tbody>
    </table>

    <div id="account-modal" class="modal">
        <div class="modal-content">
            <span class="close" onclick="toggleAccount()">&times;</span>
            <h2>Mi cuenta</h2>
            <form>
                <label for="email">Correo electrónico:</label>
                <input type="email" id="email" name="email">
                
                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="password">
                
                <button type="submit">Guardar cambios</button>
            </form>
        </div>
    </div>

    <div id="viewModal" class="modal">
    <div class="modal-content">
        <span class="close" onclick="closeModal()">&times;</span>
        <h2>Detalles del Formulario</h2>
        <div id="form-details">
            <!--se cargarán los datos del formulario usando ajax -->
        </div>
        <button onclick="revisado()">Marcar como Revisado</button>

    </div>
    </div>

    <script src="script.js"></script>
</body>
</html>


