<?php
$servername = "localhost";
$username = "root";
$password = "";

$conn = new mysqli($servername, $username, $password);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "CREATE DATABASE IF NOT EXISTS finalproject";
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully<br>";
} else {
    echo "Error creating database: " . $conn->error . "<br>";
}

$conn->select_db("finalproject");

$sql1 = "CREATE TABLE IF NOT EXISTS form (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    edad VARCHAR(3) NOT NULL,
    genero VARCHAR(10) NOT NULL,
    tel VARCHAR(20) NOT NULL,
    mail VARCHAR(50) NOT NULL,
    condicion TEXT NOT NULL,
    medicacion TEXT,
    alergia TEXT,
    tabaco VARCHAR(3) NOT NULL,
    alcohol VARCHAR(20) NOT NULL,
    fecha_envio TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    estado VARCHAR(20) DEFAULT 'Pendiente'
)";

if ($conn->query($sql1) === TRUE) {
    echo "Table 'form' created successfully<br>";
} else {
    echo "Error creating table: " . $conn->error . "<br>";
}

$sql2 = "CREATE TABLE IF NOT EXISTS users (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    mail VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    rol VARCHAR(20) NOT NULL
)";

if ($conn->query($sql2) === TRUE) {
    echo "Table 'users' created successfully<br>";
} else {
    echo "Error creating table 'users': " . $conn->error . "<br>";
}

$sql3 = "INSERT INTO users (name, mail, password, rol)
VALUES ('susana', 'ibarra@gmail.com', '123', 'admin'),
       ('user', 'ibarra@gmail.com', '123', '')";

if ($conn->query($sql3) === TRUE) {
    echo "New record created successfully for users<br>";
} else {
    echo "Error: " . $sql3 . "<br>" . $conn->error . "<br>";
}

$conn->close();
?>
