<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@300;400;500;600;700&family=LXGW+WenKai+Mono+TC&family=Noto+Sans:ital,wght@0,100..900;1,100..900&family=Raleway:ital,wght@0,100..900;1,100..900&family=Yantramanav:wght@100;300;400;500;700;900&family=Zen+Kaku+Gothic+New&display=swap" rel="stylesheet">
    <link rel="icon" type="img/png" href="img/logo.png">
    <link rel="stylesheet" href="css/form.css">
    <title>Formulario</title>
</head>
<body>
    <div class="container">
        <div class="top">
            <h1>Formulario de historial médico</h1>
        </div>
        <form action="submit_form.php" method="post">
            <div class="form-row">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input class="inp" type="text" id="nombre" name="nombre">
                </div>
                <div class="form-group">
                    <label for="apellido">Apellido</label>
                    <input class="inp" type="text" id="apellido" name="apellido">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group">
                    <label for="edad">¿Cuál es tu edad?</label>
                    <input class="inp" type="number" id="edad" name="edad" placeholder="Ej: 25">
                </div>
                <div class="form-group">
                    <label for="genero">¿Cuál es tu género?</label>
                    <select class="inp" id="genero" name="genero">>
                        <option value="">Por favor seleccione</option>
                        <option value="male">Masculino</option>
                        <option value="fem">Femenino</option>
                        <option value="otro">Otro</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group">
                    <label for="phone">Número de contacto</label>
                    <input class="inp" type="tel" id="phone" name="phone" placeholder="(000) 000-0000">
                </div>
                <div class="form-group">
                    <label for="email">Dirección de correo electrónico</label>
                    <input class="inp" type="email" id="email" name="email" placeholder="correo@ejemplo.com">
                </div>
            </div>

            <div class="form-group2">
                <label>Consulta las condiciones que aplican para ti o cualquier miembro de tu familia directa:</label>
                <div class="checkbox-group">
                    <label><input type="checkbox"name="condiciones[]" value="Asma"> Asma</label>
                    <label><input type="checkbox"name="condiciones[]" value="Cancer"> Cáncer</label>
                    <label><input type="checkbox"name="condiciones[]" value="Enfermedad cardica"> Enfermedad cardíaca</label>
                    <label><input type="checkbox"name="condiciones[]" value="Diabetes"> Diabetes</label>
                    <label><input type="checkbox"name="condiciones[]" value="Epilepsia"> Epilepsia</label>
                    <label><input type="checkbox"name="condiciones[]" value="Trastorno psiquiatrico"> Trastorno psiquiátrico</label>
                    <label><input type="checkbox"name="condiciones[]" value="Otros"> Otros</label>
                </div>
            </div>

            <div class="form-group2">
                <label>¿Está tomando alguna medicación actualmente?</label>
                <div class="radio-group">
                    <label><input type="radio" name="medicacion" value="si"> Sí</label>
                    <label><input type="radio" name="medicacion" value="no"> No</label>
                </div>
                <textarea name="medicacion_texto" placeholder="Por favor, especifique..."></textarea>
            </div>

            <div class="form-group2">
                <label>¿Tiene alguna alergia a algún medicamento?</label>
                <div class="radio-group">
                    <label><input type="radio" name="alergia" value="si"> Sí</label>
                    <label><input type="radio" name="alergia" value="no"> No</label>
                    <label><input type="radio" name="alergia" value="no-seguro"> No estoy seguro</label>
                </div>
                <textarea name="alergia_texto" placeholder="Por favor, especifique..."></textarea>
            </div>

            <div class="form-group2">
                <label>¿Consumes algún tipo de tabaco o lo has consumido alguna vez?</label>
                <div class="radio-group">
                    <label><input type="radio" name="tabaco" value="si"> Sí</label>
                    <label><input type="radio" name="tabaco" value="no"> No</label>
                </div>
            </div>
            <div class="form-group2">
                <label>¿Con qué frecuencia consumes alcohol?</label>
                <div class="radio-group">
                    <label><input type="radio" name="alcohol" value="si"> Diario</label>
                    <label><input type="radio" name="alcohol" value="no"> Semanalmente</label>
                    <label><input type="radio" name="alcohol" value="no"> Ocasionalmente</label>
                    <label><input type="radio" name="alcohol" value="no"> Nunca</label>
                </div>
            </div>
            <input class="btn" type="submit" name="action" value="Enviar">
        </form>

    </div>
    
</body>
</html>