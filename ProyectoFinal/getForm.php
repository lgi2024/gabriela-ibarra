<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "finalproject";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
    $sql = "SELECT * FROM form WHERE id = $id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div data-form-id='{$row['id']}'>";
        echo "<p><strong>Nombre:</strong> " . $row['nombre'] . " " . $row['apellido'] . "</p>";
        echo "<p><strong>Fecha de Envío:</strong> " . $row['fecha_envio'] . "</p>";
        echo "<p><strong>Estado:</strong> " . $row['estado'] . "</p>";
        echo "<p><strong>Telefono:</strong> " . $row['tel'] . "</p>"; 
        echo "<p><strong>Mail:</strong> " . $row['mail'] . "</p>"; 
        echo "<p><strong>Condicion:</strong> " . $row['condicion'] . "</p>"; 
        echo "<p><strong>Medicacion:</strong> " . $row['medicacion'] . "</p>"; 
        echo "<p><strong>Alergia:</strong> " . $row['alergia'] . "</p>"; 
        echo "<p><strong>Consume tabaco:</strong> " . $row['tabaco'] . "</p>"; 
        echo "<p><strong>Consume alcohol:</strong> " . $row['alcohol'] . "</p>"; 
        echo "</div>";
    } else {
        echo "No se encontraron datos para este formulario.";
    }
}
$conn->close();
?>
