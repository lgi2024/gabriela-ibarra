<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=LXGW+WenKai+Mono+TC&family=Yantramanav:wght@100;300;400;500;700;900&family=Zen+Kaku+Gothic+New&display=swap" rel="stylesheet">
    <link rel="icon" type="img/png" href="img/logo.png">
    <link rel="stylesheet" href="css/landingPage.css">
    <title>Dra. Susana Lopez</title>
</head>
<body>
   <div class="container">
        <div class="text-section">
            <h1>Dra. Susana Lopez</h1>
            <p>Bienvenido, aquí podrás llenar tu información médica y adjuntar documentos antes de tu visita.</p>
            <a href="form.php">Completar Formulario</a>
        </div>
    </div>
</body>
</html>
