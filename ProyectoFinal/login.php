
<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "finalproject";
$error = "";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("La conexión a la base de datos falló: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST['name'];
    $contrasena = $_POST['con'];

    $nombre = $conn->real_escape_string($nombre);
    $contrasena = $conn->real_escape_string($contrasena);

    $sql = "SELECT * FROM users WHERE name = '$nombre' AND password = '$contrasena'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $rol = $row['rol'];
        
        $_SESSION['username'] = $nombre;
        
        if ($rol == 'admin') {
            header("Location: adminPanel.php");
        } elseif ($rol == '') {
            header("Location: landingPage.php");
        } else {
            header("Location: login.php");
        }
        exit;
    } else {
        header("Location: login.php?e=required_login");
        exit;
    }
}

$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="img/png" href="img/logo.png">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="css/login.css">
    <title>Login</title>
</head>
<body>
    <div class="container">
        <div class="image-section">
            <img src="img/login.jpg" alt="login">
        </div>
        <div class="info">
           <div class= "form-info">
            <h1>Bienvenido</h1>
                <form action="#" method="post">
                    <label>
                        <i class='bx bx-user'></i>
                        <input type="text" size="25" name="name" placeholder="Nombre" required />
                    </label>
                    <label>
                        <i class='bx bx-lock-alt' ></i>
                        <input type="password" size="25" name="con" placeholder="Contraseña" required />
                    </label>
                    <input class="btn" type="submit" name="action" value="Iniciar sesión">
                </form>
                <div id="form-error" class="error hidden"></div>
           </div>
        </div>
    </div> 
    <script>
        document.addEventListener("DOMContentLoaded", e => {
            let $formError = document.getElementById('form-error');
            let querys = new URLSearchParams(window.location.search);
            let error = querys.get('e');
            if (error) {
                $formError.classList.remove('hidden');
                if (error == 'required_data')
                    $formError.textContent = 'Todos los datos son necesarios';
                else if (error == 'required_login') {
                    $formError.textContent = 'Es necesario iniciar sesión para seguir navegando.';
                } else
                    $formError.textContent = error;
            }
        });
    </script>
</body>
</html>
