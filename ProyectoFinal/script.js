function toggleAccount() {
    const modal = document.getElementById("account-modal");
    modal.style.display = (modal.style.display === "block") ? "none" : "block";
}

window.onclick = function(event) {
    const modal = document.getElementById("account-modal");
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function openModal(formId) {
     document.getElementById('viewModal').style.display = 'block';

    const xhr = new XMLHttpRequest();
    xhr.open("GET", "getForm.php?id=" + formId, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            document.getElementById('form-details').innerHTML = xhr.responseText;
        }
    };
    xhr.send();

}

function closeModal() {
    document.getElementById('viewModal').style.display = 'none';
}


function revisado() {
    const formId = document.getElementById("viewModal").dataset.formId;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "update.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
     
            const statusCell = document.querySelector(`tr[data-id="${formId}"] .status-icon`);
            statusCell.className = "status-icon green";
            statusCell.nextSibling.textContent = " Revisado";
            closeModal(); 
        }
    };

    xhr.send("id=" + formId + "&estado=Revisado");
}