<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "finalproject";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$edad = $_POST['edad'];
$genero = $_POST['genero'];
$tel = $_POST['phone'];
$mail = $_POST['email'];
$condiciones = implode(", ", $_POST['condiciones'] ?? []);
$medicacion = isset($_POST['medicacion']) && $_POST['medicacion'] === 'si' ? $_POST['medicacion_texto'] : '';
$alergia = isset($_POST['alergia']) && $_POST['alergia'] === 'si' ? $_POST['alergia_texto'] : '';
$tabaco = $_POST['tabaco'] ?? '';
$alcohol = $_POST['alcohol'] ?? '';

$sql = "INSERT INTO form (nombre, apellido, edad, genero, tel, mail, condicion, medicacion, alergia, tabaco, alcohol)
        VALUES ('$nombre', '$apellido', '$edad', '$genero', '$tel', '$mail', '$condiciones', '$medicacion', '$alergia', '$tabaco', '$alcohol')";

if ($conn->query($sql) === TRUE) {
    echo "Datos guardados correctamente.";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
