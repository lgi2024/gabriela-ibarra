<?php
include("conexion.php");
$con = connection();

// Ordenación
$order_column = isset($_GET['order_column']) ? $_GET['order_column'] : 'name';
$order_direction = isset($_GET['order_direction']) ? $_GET['order_direction'] : 'ASC';

// Búsqueda
if (isset($_GET['search']) && !empty($_GET['search'])) {
    $search = mysqli_real_escape_string($con, $_GET['search']);
    $sql = "SELECT * FROM students WHERE name LIKE '%$search%' OR email LIKE '%$search%' OR id = '$search'";
} else {
    $sql = "SELECT * FROM students";
}
$sql .= " ORDER BY $order_column $order_direction";

$query = mysqli_query($con, $sql);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Sistema de Gestión de Estudiantes</title>
</head>
<body>
    <div>
        <form action="create.php" method="POST" enctype="multipart/form-data">
            <p><a href="index.php">Cerrar sesión</a></p>
            <h2>Agregar nuevo estudiante</h2>
            <input type="text" name="name" placeholder="Nombre" required>
            <input type="text" name="age" placeholder="Edad" required>
            <input type="text" name="email" placeholder="Correo electrónico" required>
            <input type="file" name="profile_picture" accept="image/*">
            <input type="submit" value="Agregar">
        </form>
    </div>
    <div>
        <h2>Buscar estudiantes</h2>
        <form action="" method="GET">
            <input type="text" name="search" placeholder="Buscar por nombre, correo electrónico o ID">
            <input type="submit" value="Buscar">
        </form>
    </div>
    <div>
        <h2>Estudiantes registrados</h2>
        <form method="GET">
            <label for="order_column">Ordenar por:</label>
            <select name="order_column" id="order_column">
                <option value="name" <?= $order_column == 'name' ? 'selected' : '' ?>>Nombre</option>
                <option value="age" <?= $order_column == 'age' ? 'selected' : '' ?>>Edad</option>
                <option value="email" <?= $order_column == 'email' ? 'selected' : '' ?>>Correo electrónico</option>
            </select>
            <select name="order_direction" id="order_direction">
                <option value="ASC" <?= $order_direction == 'ASC' ? 'selected' : '' ?>>Ascendente</option>
                <option value="DESC" <?= $order_direction == 'DESC' ? 'selected' : '' ?>>Descendente</option>
            </select>
            <input type="submit" value="Ordenar">
        </form>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Correo electrónico</th>
                    <th>Foto de perfil</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_array($query)): ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['age'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td>
                        <?php if ($row['profile_picture']): ?>
                            <img src="img/<?= $row['profile_picture'] ?>" alt="Foto de perfil" width="50">
                        <?php else: ?>
                            Sin foto
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="update.php?id=<?= $row['id'] ?>">Editar</a>
                        <a href="delete.php?id=<?= $row['id'] ?>">Eliminar</a>
                    </td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</body>
</html>
