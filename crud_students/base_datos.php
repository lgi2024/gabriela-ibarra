<?php
$servername = "localhost";
$username = "root";
$password = "";

$conn = new mysqli($servername, $username, $password);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Crear database
$sql = "CREATE DATABASE IF NOT EXISTS school";
if ($conn->query($sql) === TRUE) {
  echo "Database created successfully<br>";
} else {
  echo "Error creating database: " . $conn->error . "<br>";
}

$conn->select_db("school");

$sql2 = "CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    age INT NOT NULL,
    email VARCHAR(100) NOT NULL,
    profile_picture VARCHAR(255) NULL
)";

$sql1 = "CREATE TABLE usuario (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30) NOT NULL,
  password VARCHAR(30) NOT NULL,
  rol VARCHAR(20) NOT NULL
)";

if ($conn->query($sql1) === TRUE) {
  echo "Table usuario created successfully<br>";
} else {
  echo "Error creating table usuario: " . $conn->error . "<br>";
}

if ($conn->query($sql2) === TRUE) {
    echo "Table students created successfully<br>";
} else {
    echo "Error creating table students: " . $conn->error . "<br>";
}

$sql3 = "INSERT INTO usuario (name, pass, rol)
VALUES ('gabi', '123', 'admin'),
       ('juan', '123', '')";

if ($conn->query($sql3) === TRUE) {
  echo "New record created successfully for usuario<br>";
} else {
  echo "Error: " . $sql3 . "<br>" . $conn->error . "<br>";
}

$conn->close();
?>
