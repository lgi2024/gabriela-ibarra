<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $age = $_POST['age'];
    $email = $_POST['email'];
    $profile_picture = $_FILES['profile_picture']['name'];

    if ($profile_picture) {
        $targetDir = __DIR__ . "/img/";
        $targetFile = $targetDir . basename($profile_picture);
        if (!move_uploaded_file($_FILES['profile_picture']['tmp_name'], $targetFile)) {
            die("Error al subir la imagen.");
        }
    }

    // Validaciones de edad y correo
    if (!is_numeric($age) || $age <= 0) {
        die("Error: La edad debe ser un número positivo.");
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        die("Error: El correo electrónico no tiene un formato válido.");
    }

    $conn = mysqli_connect('localhost', 'root', '', 'school');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    $sql = "INSERT INTO students (name, age, email, profile_picture) VALUES ('$name', $age, '$email', '$profile_picture')";

    if (mysqli_query($conn, $sql)) {
        header("Location: admin.php"); 
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
}
?>
