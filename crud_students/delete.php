<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];

    $conn = mysqli_connect('localhost', 'root', '', 'school');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    $sql = "DELETE FROM students WHERE id=$id";

    if (mysqli_query($conn, $sql)) {
        header("Location: admin.php");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
} else {
    $id = $_GET['id'];

    echo "<form method='post' action=''>
        <input type='hidden' name='id' value='$id'>
        ¿Estás seguro de que quieres eliminar al estudiante con ID: $id?<br>
        <input type='submit' value='Eliminar'>
    </form>";
}
?>
