<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $age = $_POST['age'];
    $email = $_POST['email'];
    $profile_picture = $_FILES['profile_picture']['name'];

    $conn = mysqli_connect('localhost', 'root', '', 'school');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    if ($profile_picture) {
        $targetDir = __DIR__ . "/img/";
        $targetFile = $targetDir . basename($profile_picture);
        if (!move_uploaded_file($_FILES['profile_picture']['tmp_name'], $targetFile)) {
            die("Error al subir la imagen.");
        }

        $sql = "UPDATE students SET name='$name', age=$age, email='$email', profile_picture='$profile_picture' WHERE id=$id";
    } else {
        $sql = "UPDATE students SET name='$name', age=$age, email='$email' WHERE id=$id";
    }

    if (mysqli_query($conn, $sql)) {
        header("Location: admin.php");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
} else {
    $id = $_GET['id'];

    $conn = mysqli_connect('localhost', 'root', '', 'school');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    $sql = "SELECT id, name, age, email, profile_picture FROM students WHERE id=$id";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $name = $row['name'];
        $age = $row['age'];
        $email = $row['email'];
        $profile_picture = $row['profile_picture'];
    } else {
        echo "No se encontró el estudiante con ID: $id";
        exit();
    }

    mysqli_close($conn);
}
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Actualizar Estudiante</title>
</head>
<body>
    <div>
        <form action="update.php" method="POST" enctype="multipart/form-data"> 
            <h1>Actualizar estudiante</h1>
            <input type="hidden" name="id" value="<?= $id ?>">
            <input type="text" name="name" value="<?= $name ?>" placeholder="Nombre" required>
            <input type="text" name="age" value="<?= $age ?>" placeholder="Edad" required>
            <input type="text" name="email" value="<?= $email ?>" placeholder="Correo electrónico" required>
            <input type="submit" value="Actualizar">
            
            <p>Foto de perfil actual:</p>
            <?php if ($profile_picture): ?>
                <img src="img/<?= $profile_picture ?>" alt="Foto de perfil" width="100"><br>
            <?php else: ?>
                Sin foto<br>
            <?php endif; ?>

            <p>Cambiar foto de perfil:</p>
            <input type="file" name="profile_picture" accept="image/*"><br>
        </form>
    </div>
</body>
</html>
